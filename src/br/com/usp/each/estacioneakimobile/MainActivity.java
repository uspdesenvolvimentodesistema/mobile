package br.com.usp.each.estacioneakimobile;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import br.com.usp.each.estacioneakimobile.domain.Aluguel;
import br.com.usp.each.estacioneakimobile.domain.Cliente;
import br.com.usp.each.estacioneakimobile.domain.Estacionamento;
import br.com.usp.each.estacioneakimobile.domain.StatusVaga;
import br.com.usp.each.estacioneakimobile.domain.Vaga;
import br.com.usp.each.estacioneakimobile.service.ClienteService;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity {
	// Google Map
	private GoogleMap googleMap;
	private Cliente clienteCorrente;
	private Aluguel aluguelCorrente;

	private ProgressDialog mprogressDialog;
	HashMap<String, Estacionamento> mapEstacionamento = new HashMap<String, Estacionamento>();
	
	HashMap<StatusVaga, Integer> mapIcone = new HashMap<StatusVaga, Integer>();
	{
		mapIcone.put(StatusVaga.DISPONIVEL, R.drawable.estacionamento_com_vaga);
		mapIcone.put(StatusVaga.OCUPADO, R.drawable.estacionamento_sem_vaga);
		mapIcone.put(StatusVaga.RESERVADO,
				R.drawable.estacionamento_vaga_agendada);
	}

	Marker markerCorrente = null;
	Estacionamento estacionamentoAgendado;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(br.com.usp.each.estacioneakimobile.R.layout.activity_main);

		
		Intent it = getIntent();
		
		clienteCorrente = (Cliente) it.getExtras().get(Cliente.class.getName());
		new ServiceVerificarPendencia().execute();
		
		Button btnReserva = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnReserva);
		Button btnEditar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnEditar);
		Button btnSair = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnSair);
		
		btnReserva.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(aluguelCorrente == null){
					Toast.makeText(MainActivity.this,
							"Nenhuma Reserva efetuada", Toast.LENGTH_SHORT)
							.show();
				}
				else{
					
					Intent it = new Intent(MainActivity.this,BarCodeActivity.class);
					it.putExtra(Estacionamento.class.getName(), estacionamentoAgendado);
					it.putExtra(Aluguel.class.getName(), aluguelCorrente);
					it.putExtra("Codigo", aluguelCorrente.getId()+"."+
							aluguelCorrente.getVaga().getId()+"."+ aluguelCorrente.getVeiculo().getId());
					
					startActivity(it);
					
				}
				
			}
		});
		
		btnEditar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clienteCorrente == null){
					Toast.makeText(MainActivity.this,
							"Ocorreu algum erro, tente mais tarde", Toast.LENGTH_SHORT)
							.show();
					
					finish();
				}
				else{
					Intent it = new Intent(MainActivity.this,EditarActivity.class);
					it.putExtra(Cliente.class.getName(), clienteCorrente);
					startActivity(it);
					finish();
				}
				
			}
		});
		
		btnSair.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		new Thread() {
			@SuppressWarnings("static-access")
			@Override
			public void run() {
				while (true) {
					
					new ServiceVerificarPendencia().execute();
					new ServiceGetEstacionamento().execute();
					try {
						new Thread().sleep(5000l);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}.start();

		try {
			// Loading map
			initilizeMap();

			// Setting a custom info window adapter for the google map
			googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
				@Override
				public View getInfoWindow(Marker marker) {
					// Use default InfoWindow frame
					return null;

				}

				// Defines the contents of the InfoWindow
				@Override
				public View getInfoContents(Marker marker) {

					// Getting view from the layout file info_window_layout
					View v;
					LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
					v = inflator.inflate(br.com.usp.each.estacioneakimobile.R.layout.marker_info,null);

					TextView estacionamento = (TextView) v
							.findViewById(br.com.usp.each.estacioneakimobile.R.id.txtEstacionamento);

					TextView vagas = (TextView) v
							.findViewById(br.com.usp.each.estacioneakimobile.R.id.txtVagas);
					
					TextView valor1hora = (TextView) v
							.findViewById(br.com.usp.each.estacioneakimobile.R.id.txtValor1hora);
					TextView valorDemaisHoras = (TextView) v
							.findViewById(br.com.usp.each.estacioneakimobile.R.id.txtValorDemaishora);
					

					Button btnAgendar = (Button) v
							.findViewById(br.com.usp.each.estacioneakimobile.R.id.btnAgendar);
					 
						btnAgendar.setEnabled(true);
						if (estacionamentoAgendado!=null && estacionamentoAgendado.getCNPJ().equals(mapEstacionamento
								.get(marker.getTitle()).getCNPJ())) {
							
							if(aluguelCorrente!=null && aluguelCorrente.getVaga().getStatusVaga().equals(StatusVaga.OCUPADO)){
								btnAgendar.setEnabled(false);
								btnAgendar.setText("Voc� esta aqui");
							}
							else
								btnAgendar.setText("Cancelar");
							
						}else if (marker.getSnippet().equals("0") ) {
							btnAgendar.setEnabled(false);
							btnAgendar.setText("Sem Vagas");
						}
						else {
							btnAgendar.setText("Reservar");
						}
					
				
							valor1hora.setText("1 hora: R$"+ new DecimalFormat("0.00").format(mapEstacionamento
									.get(marker.getTitle()).getValor1hora()).replace(",","."));
							valorDemaisHoras.setText("demais horas: R$"+ new DecimalFormat("0.00").format(mapEstacionamento
									.get(marker.getTitle()).getValorDemaisHoras()).replace(",","."));
						
				
					estacionamento.setText("Estacionamento: "
							+ marker.getTitle());

	
					vagas.setText("Vagas Dispon�veis: " + marker.getSnippet());
					
					
					markerCorrente = marker;

					// Returning the view containing InfoWindow contents

					return v;
				}
			});
			googleMap
					.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

						@Override
						public void onInfoWindowClick(Marker marker) {
							System.out.println(marker.getTitle());
							markerCorrente = marker;
							
							if (estacionamentoAgendado!=null || !marker.getSnippet().equals("0")) {
								if(estacionamentoAgendado!=null &&(!estacionamentoAgendado.getCNPJ().equals(mapEstacionamento.get(marker.getTitle()).getCNPJ()))){
									informacao();
								}
								else
								confirmacao();
							}

						}
					});
			googleMap.setMyLocationEnabled(true);
			LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			Criteria criteria = new Criteria();

			String provider = locationManager.getBestProvider(criteria, true);

			Location location = locationManager.getLastKnownLocation(provider);

			if (location == null) {

				com.google.android.gms.location.LocationListener locationListener = new com.google.android.gms.location.LocationListener() {

					@Override
					public void onLocationChanged(Location location) {
						double lat = location.getLatitude();
						double lng = location.getLongitude();
						LatLng ll = new LatLng(lat, lng);
						googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
								ll, 16));

					}
				};

			} else {
				double lat = location.getLatitude();
				double lng = location.getLongitude();

				LatLng ll = new LatLng(lat, lng);

				googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 16));
			}

			// googleMap.moveCamera(CameraUpdateFactory.));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * function to load map. If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					br.com.usp.each.estacioneakimobile.R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}

	public void confirmacao() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					if (estacionamentoAgendado == null) {
						estacionamentoAgendado = mapEstacionamento.get(markerCorrente.getTitle());
						new ServiceAgendarVaga().execute(estacionamentoAgendado);
						new ServiceVerificarPendencia().execute();
						
					} else{
						new ServiceCancelarVaga().execute(aluguelCorrente);
						estacionamentoAgendado = null;
						new ServiceVerificarPendencia().execute();
					}
					break;
				case DialogInterface.BUTTON_NEGATIVE:

					break;
				}
			}
		};
		
		

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if (estacionamentoAgendado == null) {
			builder.setMessage("Tem certeza que deseja reservar?")
					.setPositiveButton("SIM", dialogClickListener)
					.setNegativeButton("N�O", dialogClickListener).show();
		} else {
			builder.setMessage("Tem certeza que deseja cancelar sua reserva?")
					.setPositiveButton("SIM", dialogClickListener)
					.setNegativeButton("N�O", dialogClickListener).show();
		}

	}
	
	public void informacao() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					break;
			}
		}};
		
		

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Voc� j� possui uma reserva no Estacionamento :"+estacionamentoAgendado.getNome())
					.setPositiveButton("Ok", dialogClickListener).show();

	
	}

	class ServiceGetEstacionamento extends
			AsyncTask<Void, String, List<Estacionamento>> {

		@Override
		protected List<Estacionamento> doInBackground(Void... params) {
			try {
				return new ClienteService().getEstacionamento();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<Estacionamento> result) {

			super.onPostExecute(result);
			googleMap.clear();
			if (result != null && !result.isEmpty()) {
					setMarkers(result);
				
			}
		}

		private void setMarkers(List<Estacionamento> result) {
			double latitude;
			double longitude;
			estacionamentoAgendado = null;
			for (Estacionamento e : result) {
				if (e != null) {
					String localizacao[] = e.getCoordenadas().split(";");
					latitude = Double.parseDouble(localizacao[0]);
					longitude = Double.parseDouble(localizacao[1]);
					int vagasDiponiveis = 0;
					
					int imagem = 0;
					
					if (e.getVagaDisponivel()) {
						imagem = mapIcone.get(StatusVaga.DISPONIVEL);
					} 
					else{
						imagem = mapIcone.get(StatusVaga.OCUPADO);
					}
					
					for(Vaga v: e.getVagas()){
						if(v.getStatusVaga().equals(StatusVaga.DISPONIVEL)){
							vagasDiponiveis++;
						}
						else if(v.getStatusVaga().equals(StatusVaga.RESERVADO)){
							if(aluguelCorrente.getVaga().getId()==v.getId()){
								imagem = mapIcone.get(StatusVaga.RESERVADO);
								estacionamentoAgendado = e;
								
							}
						}
						else if(v.getStatusVaga().equals(StatusVaga.OCUPADO)){
							if(aluguelCorrente!=null && aluguelCorrente.getVaga().getId()==v.getId()){
								imagem = mapIcone.get(StatusVaga.OCUPADO);
								estacionamentoAgendado = e;
								
							}
						}
						
					}
					MarkerOptions marker = new MarkerOptions()
							.position(new LatLng(latitude, longitude))
							.title(e.getNome())
							.snippet(String.valueOf(vagasDiponiveis))
							.icon(BitmapDescriptorFactory
									.fromResource(imagem));
					googleMap.addMarker(marker);
					mapEstacionamento.put(marker.getTitle(), e);
				}
			}
		}
	}
	
	class ServiceVerificarPendencia extends
	AsyncTask<Void, String, Aluguel> {

		@Override
		protected Aluguel doInBackground(Void... params) {
			try {
				aluguelCorrente = new ClienteService().verificarPendenciaPorVeiculo(clienteCorrente.getVeiculos().get(0));
			} catch (Exception e) {
				
				e.printStackTrace();
				return null;
			}

			return aluguelCorrente;
		}
		
		@Override
		protected void onPostExecute(Aluguel result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}

	}
	
	class ServiceAgendarVaga extends
	AsyncTask<Estacionamento, String, String> {

		@Override
		protected String doInBackground(Estacionamento... estacionamento) {
			try {
				return  new ClienteService().reservarVaga(clienteCorrente.getVeiculos().get(0), estacionamento[0]);
			} catch (Exception e) {
				
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(MainActivity.this, "Aguarde", "Processando...");
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();;
			mprogressDialog.dismiss();
		}
	}
		class ServiceCancelarVaga extends
		AsyncTask<Aluguel, String, String> {

			@Override
			protected String doInBackground(Aluguel... aluguel) {
				try {
					return  new ClienteService().cancelarVaga(aluguel[0]);
				} catch (Exception e) {
					
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				mprogressDialog = ProgressDialog.show(MainActivity.this, "Aguarde", "Processando...");
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				Toast.makeText(MainActivity.this, result, Toast.LENGTH_LONG).show();;
				mprogressDialog.dismiss();
			}
		}
	
	
}
