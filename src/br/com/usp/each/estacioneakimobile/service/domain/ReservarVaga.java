package br.com.usp.each.estacioneakimobile.service.domain;

import br.com.usp.each.estacioneakimobile.domain.Estacionamento;
import br.com.usp.each.estacioneakimobile.domain.Veiculo;

public class ReservarVaga {

	private Veiculo veiculo;
	private Estacionamento estacionamento;
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public Estacionamento getEstacionamento() {
		return estacionamento;
	}
	public void setEstacionamento(Estacionamento estacionamento) {
		this.estacionamento = estacionamento;
	}
	
	
}
