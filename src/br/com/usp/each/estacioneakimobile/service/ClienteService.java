package br.com.usp.each.estacioneakimobile.service;

import java.util.ArrayList;
import java.util.List;

import br.com.usp.each.estacioneakimobile.domain.Aluguel;
import br.com.usp.each.estacioneakimobile.domain.Cliente;
import br.com.usp.each.estacioneakimobile.domain.Estacionamento;
import br.com.usp.each.estacioneakimobile.domain.Marca;
import br.com.usp.each.estacioneakimobile.domain.Veiculo;
import br.com.usp.each.estacioneakimobile.service.domain.ReservarVaga;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ClienteService {
    // EX: 200.153.36.140:8070
//  private static String URI = "http://50.112.183.101:8081/WS/json/cliente/";
  private static String URI = "http://192.168.1.43:8080/servicos_cliente/cliente/"; 
//  private static String URI = "http://192.168.1.10:8080/WS/json/cliente/"; 
//  private static String URIEstacionamento = "http://50.112.183.101:8081/WS/json/estacionamento/";
  private static String URIEstacionamento = "http://192.168.1.43:8080/servicos_estacionamento/estacionamento/";
//  private static String URIEstacionamento = "http://192.168.1.10:8080/WS/json/estacionamento/";


  public Cliente Logar(Cliente cliente) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  String userJSONString = gson.toJson(cliente);
	  String[] json = new WebService().post(URI + "login",userJSONString);

	  Cliente clienteCorrente = new Cliente();

      if (json[0].equals("200")) {
 
          clienteCorrente = gson.fromJson(json[1],Cliente.class);
                   
          return clienteCorrente;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  public String reservarVaga(Veiculo veiculo, Estacionamento estacionamento) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  ReservarVaga reserva = new ReservarVaga();
	  reserva.setVeiculo(veiculo);
	  reserva.setEstacionamento(estacionamento);
	  String userJSONString = gson.toJson(reserva);
	  String[] json = new WebService().post(URI + "reservarvaga",userJSONString);

	  String retorno;

      if (json[0].equals("200")) {
          // Fazendo o parse do JSON para um JsonArray
    	  retorno = gson.fromJson(json[1],String.class);
          return retorno;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  public Cliente atualizar(Cliente cliente) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	 
	  String userJSONString = gson.toJson(cliente);
	  String[] json = new WebService().post(URI + "atualizacao",userJSONString);

	  Cliente retorno;

      if (json[0].equals("200")) {
          // Fazendo o parse do JSON para um JsonArray
    	  retorno = gson.fromJson(json[1],Cliente.class);
          return retorno;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  public Cliente cadastro(Cliente cliente) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	 
	  String userJSONString = gson.toJson(cliente);
	  String[] json = new WebService().post(URI + "cadastro",userJSONString);

	  Cliente retorno;

      if (json[0].equals("200")) {
          // Fazendo o parse do JSON para um JsonArray
    	  retorno = gson.fromJson(json[1],Cliente.class);
          return retorno;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  
  public String cancelarVaga(Aluguel aluguel) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  String userJSONString = gson.toJson(aluguel);
	  String[] json = new WebService().post(URI + "cancelarvaga",userJSONString);

	  String retorno;

      if (json[0].equals("200")) {
          // Fazendo o parse do JSON para um JsonArray
    	  
    	  
    	  retorno = gson.fromJson(json[1],String.class);
          return retorno;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  
  public Aluguel verificarPendenciaPorVeiculo(Veiculo veiculo) throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  String userJSONString = gson.toJson(veiculo);
	  String[] json = new WebService().post(URI + "verificarpendenciaveiculo",userJSONString);

	  Aluguel retorno =null;

      if (json[0].equals("200")) {
          // Fazendo o parse do JSON para um JsonArray
    	  retorno = gson.fromJson(json[1],Aluguel.class);
          return retorno;

      } else {
          throw new Exception(json[1]);
      }

  }
  
  public List<Estacionamento> getEstacionamento() throws Exception {
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  String[] json = new WebService().get(URIEstacionamento + "get");

	  List<Estacionamento> estacionamentos = new ArrayList<Estacionamento>();

      if (json[0].equals("200")) {
    	

    	 if(parser.parse(json[1]).isJsonArray()){
	         JsonArray array = parser.parse(json[1]).getAsJsonArray();
	          for(int i=0 ; i<array.size();i++){
	        	  Estacionamento esta = gson.fromJson(array.get(i), Estacionamento.class);
	        	  estacionamentos.add(esta);
	          }
    	 }
    	 else{
    		  JsonParser jParser = new JsonParser();
              JsonObject jObject = (JsonObject) jParser.parse(json[1]);
              JsonElement elem = jObject.get("estacionamento");

              Estacionamento[] enums = gson.fromJson(json[1], Estacionamento[].class);
              if(parser.parse(elem.toString()).isJsonArray()){
     	         JsonArray array = parser.parse(elem.toString()).getAsJsonArray();
     	          for(int i=0 ; i<array.size();i++){
     	        	  Estacionamento esta = gson.fromJson(parser.parse(array.get(i).toString()).getAsJsonObject(), Estacionamento.class);
     	          }
         	 }
    	 }
          return estacionamentos;
      } else {
          throw new Exception(json[1]);
      }

  }
  
  public List<Marca> getMarcas() throws Exception{
	  JsonParser parser = new JsonParser();
	  Gson gson = new Gson();
	  String[] json = new WebService().get(URI + "getmarcas");

	  List<Marca> marcas = new ArrayList<Marca>();

      if (json[0].equals("200")) {
    	

    	 if(parser.parse(json[1]).isJsonArray()){
	         JsonArray array = parser.parse(json[1]).getAsJsonArray();
	          for(int i=0 ; i<array.size();i++){
	        	  Marca esta = gson.fromJson(array.get(i), Marca.class);
	        	  marcas.add(esta);
	          }
    	 }
    	 else{
    		  JsonParser jParser = new JsonParser();
              JsonObject jObject = (JsonObject) jParser.parse(json[1]);
              JsonElement elem = jObject.get("marca");

              Estacionamento[] enums = gson.fromJson(json[1], Estacionamento[].class);
              if(parser.parse(elem.toString()).isJsonArray()){
     	         JsonArray array = parser.parse(elem.toString()).getAsJsonArray();
     	          for(int i=0 ; i<array.size();i++){
     	        	  Estacionamento esta = gson.fromJson(parser.parse(array.get(i).toString()).getAsJsonObject(), Estacionamento.class);
     	          }
         	 }
    	 }
          return marcas;
      } else {
          throw new Exception(json[1]);
      }

  }
  

}
