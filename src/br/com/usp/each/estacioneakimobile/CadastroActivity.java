package br.com.usp.each.estacioneakimobile;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.usp.each.estacioneakimobile.domain.Cliente;
import br.com.usp.each.estacioneakimobile.domain.Marca;
import br.com.usp.each.estacioneakimobile.domain.Modelo;
import br.com.usp.each.estacioneakimobile.domain.Tamanho;
import br.com.usp.each.estacioneakimobile.domain.Veiculo;
import br.com.usp.each.estacioneakimobile.service.ClienteService;

public class CadastroActivity extends Activity {
	
	ClienteService web = new ClienteService();
	private ProgressDialog mprogressDialog;
	List<Marca> marcas;
	private EditText txtNome;
	private EditText txtCpf;
	private EditText txtSenha;
	private EditText txtConfirmacaoSenha;
	private EditText txtPlaca;
	private EditText txtCor;
	private EditText txtTelefone;
	private EditText txtEmail;
	private Spinner spMarca;
	private Spinner spModelo;
	private Button btnCadastrar;
	private Button btnCancelar;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(br.com.usp.each.estacioneakimobile.R.layout.cadastro);
        txtNome = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtNome);
        txtCpf = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtCpf);
        txtSenha = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtSenha);
        txtConfirmacaoSenha = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtConfirmacaoSenha);
        txtPlaca = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtPlaca);
        txtTelefone = (EditText)findViewById(br.com.usp.each.estacioneakimobile.R.id.txtTelefone);
        txtEmail = (EditText)findViewById(br.com.usp.each.estacioneakimobile.R.id.txtEmail);
        txtCor = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtCor);
        spMarca = (Spinner) findViewById(br.com.usp.each.estacioneakimobile.R.id.spMarca);
        spModelo = (Spinner) findViewById(br.com.usp.each.estacioneakimobile.R.id.spModelo);
        btnCadastrar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnCadastrar);
        btnCancelar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnCancelar);
        new ServiceGetMarcaeModelo().execute();
   
        
        spMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				setSpModelo(position);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
        	
		});
   
        
        btnCancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				cancelar();
				
				finish();
			}
		});
        
        btnCadastrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Cliente c = new Cliente();
				c.setNome(txtNome.getText().toString());
				c.setCpf_cnpj(txtCpf.getText().toString());
				c.setTelefone(txtTelefone.getText().toString());
				if(txtSenha.getText().toString().equals(txtConfirmacaoSenha.getText().toString())){
					c.setSenha(txtSenha.getText().toString());
					c.setStatus(0);
					c.setEmail(txtEmail.getText().toString());
					Veiculo veiculo = new Veiculo();
					veiculo.setCor(txtCor.getText().toString());
					veiculo.setMarca(marcas.get(spMarca.getSelectedItemPosition()));
					
					veiculo.setModelo(getModeloporMarca(veiculo.getMarca(), spModelo.getSelectedItem().toString()));
					veiculo.setPlaca(txtPlaca.getText().toString());
					veiculo.setTipo(Tamanho.SEDAN);
					c.getVeiculos().add(veiculo);
					
					if(c.getNome().length()>1 && c.getSenha().length()>1 && c.getCpf_cnpj().length()>9
							&& c.getEmail().length()>1 && c.getTelefone().length()>1 &&
							c.getVeiculos().get(0).getCor().length()>1 &&
							c.getVeiculos().get(0).getPlaca().length()>1){
						new ServiceCadastro().execute(c);
					}
					else{
						Toast.makeText(CadastroActivity.this, "Todos os campos devem ser preenchidos",Toast.LENGTH_SHORT).show();
					}
				}
				else{
					Toast.makeText(CadastroActivity.this, "A senha e a confirma��o de senha est�o diferentes",Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	
	public Modelo getModeloporMarca(Marca marca, String nome){ 
		for(Modelo m : marca.getModelo()){
			if(m.getModelo().equals(nome)){
				return m;
			}
		}
		return null;
	}
	public void cancelar(){
		txtNome.setText("");
		txtCpf.setText("");
		txtSenha.setText("");
		txtConfirmacaoSenha.setText("");
		txtPlaca.setText("");
		txtCor.setText("");
	}
	
	public void setSpModelo(int idMarca){
		List<String> modeloString = new ArrayList<String>();
		if(marcas!=null){
			for(Modelo m: marcas.get(idMarca).getModelo()){
				modeloString.add(m.getModelo());
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, modeloString);
			arrayAdapter.notifyDataSetChanged();
			spModelo.setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		}
	}
	
	public void setSpMarca(){
		List<String> marcasString = new ArrayList<String>();
		if(marcas !=null){
			for(Marca m: marcas){
				marcasString.add(m.getMarca());
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, marcasString);
			spMarca.setAdapter(arrayAdapter);
		}
	}
	
	class ServiceGetMarcaeModelo extends AsyncTask<Void, String, List<Marca>>{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(CadastroActivity.this, "Aguarde", "Processando...");
		}

		
		@Override
		protected void onPostExecute(List<Marca> result) {
			
			super.onPostExecute(result);
			marcas = result;
			  setSpMarca();
			mprogressDialog.dismiss();
		}

		@Override
		protected List<Marca> doInBackground(Void... params) {
			List<Marca> result = null;
			try {
				result = web.getMarcas();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
	}
	
	private Cliente clienteCadastrado;
	
	class ServiceCadastro extends AsyncTask<Cliente, String, Cliente>{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(CadastroActivity.this, "Aguarde", "Processando...");
		}

		
		@Override
		protected void onPostExecute(Cliente result) {
			
			super.onPostExecute(result);
			clienteCadastrado = result;
			if(clienteCadastrado!=null){
				if(clienteCadastrado.getId().equals(-1) || clienteCadastrado.getId()<0){
					Toast.makeText(CadastroActivity.this, "CPF j� cadastrado",Toast.LENGTH_SHORT).show();
				}
				else{
					cancelar();
					Toast.makeText(CadastroActivity.this, "Cadastro com sucesso",Toast.LENGTH_SHORT).show();
					finish();
				}
			}
			else{
				Toast.makeText(CadastroActivity.this, "Ocorreu algum erro,tente novamente",Toast.LENGTH_SHORT).show();;
			}
			mprogressDialog.dismiss();
		}


		@Override
		protected Cliente doInBackground(Cliente... params) {
			Cliente result = null;
			try {
				result = web.cadastro(params[0]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return result;
		}
		
	}
}
