package br.com.usp.each.estacioneakimobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.usp.each.estacioneakimobile.domain.Aluguel;
import br.com.usp.each.estacioneakimobile.domain.Estacionamento;
import br.com.usp.each.estacioneakimobile.domain.Vaga;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

public class BarCodeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(br.com.usp.each.estacioneakimobile.R.layout.codigo_barras);
    Intent it = getIntent();
	String barcode_data = (String) it.getExtras().get("Codigo");
	Estacionamento estacionamento = (Estacionamento) it.getExtras().get(Estacionamento.class.getName());
	Aluguel aluguel = (Aluguel) it.getExtras().get(Aluguel.class.getName());
	
	ImageView iv = (ImageView) findViewById(br.com.usp.each.estacioneakimobile.R.id.imgBarCode);
	TextView lblEstacionamento = (TextView)findViewById(br.com.usp.each.estacioneakimobile.R.id.lblEstacionamento);
	TextView lblVaga = (TextView)findViewById(br.com.usp.each.estacioneakimobile.R.id.lblVaga);
	Button btnVoltar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnVoltar);
	
	btnVoltar.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			finish();
			
		}
	});
	if(estacionamento!=null)
		lblEstacionamento.setText("Estacionamento: "+estacionamento.getNome());
	if(aluguel!=null)
		lblVaga.setText("Vaga "+getIndentificadorVaga(aluguel.getVaga().getId(), estacionamento));
    Bitmap bitmap = null;
    try {

        new BarCode();
		bitmap = BarCode.encodeAsBitmap(barcode_data, BarcodeFormat.CODE_128, 600, 300);
        iv.setImageBitmap(bitmap);

    } catch (WriterException e) {
        e.printStackTrace();
    }


    }
  
    public int getIndentificadorVaga(long idvaga, Estacionamento estacionamento){
    	int contador =0;
    	
    	for(Vaga v: estacionamento.getVagas()){
    		if(v.getId().equals(idvaga)){
    			return contador+1;
    		}
    		contador++;
    	}
    	return contador+1;
    }

}