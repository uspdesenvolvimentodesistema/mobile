package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;



public class Vaga implements Dominio,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String coordenadas;
	private Integer status;

	private Tamanho tamanho;
	
	private StatusVaga statusVaga;
	
	
	public StatusVaga getStatusVaga() {
		return statusVaga;
	}
	
	public void setStatusVaga(StatusVaga statusVaga) {
		this.statusVaga = statusVaga;
	}
	
	public Tamanho getTamanho() {
		return tamanho;
	}
	
	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	
}
