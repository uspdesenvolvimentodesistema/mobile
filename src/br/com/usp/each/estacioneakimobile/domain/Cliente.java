package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Cliente implements Dominio, Serializable{
	private static final long serialVersionUID = 1L;
	

private Long id;
	
	private String nome;

	private String cpf_cnpj;
	
	private String senha;
	
	private String telefone;

	private String email;
	
	
	private int status;

	private List<Veiculo> veiculos;
	
	public List<Veiculo> getVeiculos() {
		if(veiculos ==null){
			veiculos = new ArrayList<Veiculo>();
		}
		return veiculos;
	}
	
	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		
		this.nome = nome;
	}

public String getCpf_cnpj() {
	return cpf_cnpj;
}

public void setCpf_cnpj(String cpf_cnpj) {
	this.cpf_cnpj = cpf_cnpj;
}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		try {
			if(senha==null){
				this.senha = null;
			}
			else if(senha.length()>0){
				String original = senha;
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(original.getBytes());
				byte[] digest = md.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
					sb.append(String.format("%02x", b & 0xff));
				}
				this.senha = sb.toString();
			}
			else{
				this.senha = senha;
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
}
