package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Estacionamento implements Dominio, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String login;
	
	private String senha;
	
	private String nome;

	private String coordenadas;

	private Double valor1hora;

	private Double valorDemaisHoras;

	private String CNPJ;

	private String IE;

	private List<Vaga> vagas;
	
	public List<Vaga> getVagas() {
		if(vagas==null){
			vagas= new ArrayList<Vaga>();
		}
		return vagas;
	}
	
	public boolean getVagaDisponivel(){
		for(Vaga v: vagas){
			if(v.getStatusVaga().equals(StatusVaga.DISPONIVEL)){
				return true;
			}
		}
		
		return false;
	}
	public void setVagas(List<Vaga> vagas) {
		this.vagas = vagas;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public Double getValor1hora() {
		return valor1hora;
	}

	public void setValor1hora(Double valor1hora) {
		this.valor1hora = valor1hora;
	}

	public Double getValorDemaisHoras() {
		return valorDemaisHoras;
	}

	public void setValorDemaisHoras(Double valorDemaisHoras) {
		this.valorDemaisHoras = valorDemaisHoras;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	public String getIE() {
		return IE;
	}

	public void setIE(String iE) {
		IE = iE;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setSenha(String senha) {
		try {
			if(senha.length()>0){
				String original = senha;
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(original.getBytes());
				byte[] digest = md.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
					sb.append(String.format("%02x", b & 0xff));
				}
				this.senha = sb.toString();
			}
			else{
				this.senha = senha;
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
