package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;



public class Modelo implements Dominio,Serializable{
	private static final long serialVersionUID = 1L;

	private long id;
	private String modelo;

	public String getModelo() {
		return modelo;
	}
	 
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
