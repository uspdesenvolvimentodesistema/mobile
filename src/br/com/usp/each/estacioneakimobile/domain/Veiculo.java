package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;


public class Veiculo implements Dominio, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String placa;
	private String cor;
	private Marca marca;
	private Modelo modelo;
	
	private Tamanho tipo;

	public Marca getMarca() {
		return marca;
	}
	
	public Modelo getModelo() {
		return modelo;
	}
	
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	
	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCor() {
		return cor;
	}
	
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	

	public Tamanho getTipo() {
		return tipo;
	}
	
	public void setTipo(Tamanho tipo) {
		this.tipo = tipo;
	}
	
	public String getPlaca() {
		return placa;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
}
