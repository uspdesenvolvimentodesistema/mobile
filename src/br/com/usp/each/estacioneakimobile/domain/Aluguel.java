package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;
import java.util.Calendar;

public class Aluguel implements Dominio ,Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Calendar dataHoraInicial;

	private Calendar dataHoraFinal;

	private Vaga vaga;

	private Veiculo veiculo;

	private Pagamento pagamento;
	
	private TipoAluguel tipoAluguel;
	
	public TipoAluguel getTipoAluguel() {
		return tipoAluguel;
	}
	
	public void setTipoAluguel(TipoAluguel tipoAluguel) {
		this.tipoAluguel = tipoAluguel;
	}
	
	
	public Pagamento getPagamento() {
		return pagamento;
	}
	
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	
	public Calendar getDataHoraInicial() {
		return dataHoraInicial;
	}
	public void setDataHoraInicial(Calendar dataHoraInicial) {
		this.dataHoraInicial = dataHoraInicial;
	}
	public Calendar getDataHoraFinal() {
		return dataHoraFinal;
	}
	public void setDataHoraFinal(Calendar dataHoraFinal) {
		this.dataHoraFinal = dataHoraFinal;
	}
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
