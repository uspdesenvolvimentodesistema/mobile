package br.com.usp.each.estacioneakimobile.domain;


public enum Tamanho {

	MOTO(1), MINI(2), HATCH(3), SEDAN(4), MINIVAN(5), VAN(6), PICAPE(7);

	private Integer tamanho;

	Tamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getTamanho() {
		return this.tamanho;
	}
}
