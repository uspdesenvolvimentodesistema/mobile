package br.com.usp.each.estacioneakimobile.domain;

public enum TipoAluguel {

	ALUGUEL(1), RESERVA(2);
	
	private Integer tipo;
	
	private TipoAluguel(Integer tamanho) {
		this.tipo = tamanho;
	}
	
	public Integer getTamanho() {
		return tipo;
	}
}
