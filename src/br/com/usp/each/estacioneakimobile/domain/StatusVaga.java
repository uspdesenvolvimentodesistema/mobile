package br.com.usp.each.estacioneakimobile.domain;

public enum StatusVaga {

	DISPONIVEL(0),OCUPADO(1),RESERVADO(2);
	
	private Integer tamanho;

	StatusVaga(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getTamanho() {
		return this.tamanho;
	}
}