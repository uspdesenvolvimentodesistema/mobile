package br.com.usp.each.estacioneakimobile.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Marca implements Dominio ,Serializable{
	private static final long serialVersionUID = 1L;


	private Long id;
	private String marca;
	private List<Modelo> modelo;
	
	public String getMarca() {
		return marca;
	}
	public List<Modelo> getModelo() {
		if(modelo ==null){
			modelo = new ArrayList<Modelo>();
		}
		return modelo;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public void setModelo(List<Modelo> modelo) {
		this.modelo = modelo;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
