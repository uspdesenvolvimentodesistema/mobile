package br.com.usp.each.estacioneakimobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.usp.each.estacioneakimobile.domain.Cliente;
import br.com.usp.each.estacioneakimobile.service.ClienteService;

public class LoginActivity extends Activity {
	ClienteService web = new ClienteService();
	private EditText txtLogin;
	private EditText txtSenha;
	private ProgressDialog mprogressDialog;
	
	
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(br.com.usp.each.estacioneakimobile.R.layout.login);
        Button entrar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnLogar);
        txtLogin = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtLogin);
        txtSenha= (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtSenha);
        Button cadastrar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnCadastro);
        
        
        OnClickListener btnEventoLogar =		new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(txtLogin.getText().toString().length()<10 || txtSenha.getText().toString().length() <3){
					Toast.makeText(getApplicationContext(), 
                            "Os campos Login e Senha devem ser preenchidos", Toast.LENGTH_SHORT).show();
				}
				else{
					Cliente cliente = new Cliente();
					cliente.setCpf_cnpj(txtLogin.getText().toString());
					cliente.setSenha(txtSenha.getText().toString());
					new ServiceLogin().execute(cliente);
				}
			}
		};
		
		 OnClickListener btnEventoCadastrar =		new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent it = new Intent(LoginActivity.this,CadastroActivity.class);
					startActivity(it);
					
				}
			};
		
		entrar.setOnClickListener(btnEventoLogar);
		
		cadastrar.setOnClickListener(btnEventoCadastrar);
	}
	
	class ServiceLogin extends AsyncTask<Cliente, String, Cliente>{

		@Override
		protected Cliente doInBackground(Cliente... params) {
			Cliente result = null;
			try {
				result = web.Logar(params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(LoginActivity.this, "Aguarde", "Processando...");
		}

		
		@Override
		protected void onPostExecute(Cliente result) {
			
			super.onPostExecute(result);
			if(result==null || result.getId()==null){
				Toast.makeText(getApplicationContext(), 
                        "Verifique sua conectividade com a internet, ou seu usu�rio e senha", Toast.LENGTH_SHORT).show();
			}
			else{
				txtSenha.setText("");
				txtLogin.setText("");
				Intent it = new Intent(LoginActivity.this,MainActivity.class);
				it.putExtra(Cliente.class.getName(), result);
				startActivity(it);
				
			}
			
			mprogressDialog.dismiss();
		}
		
	}


}
