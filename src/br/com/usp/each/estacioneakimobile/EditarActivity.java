package br.com.usp.each.estacioneakimobile;

import java.util.ArrayList;
import java.util.List;

import br.com.usp.each.estacioneakimobile.CadastroActivity.ServiceCadastro;
import br.com.usp.each.estacioneakimobile.CadastroActivity.ServiceGetMarcaeModelo;
import br.com.usp.each.estacioneakimobile.domain.Cliente;
import br.com.usp.each.estacioneakimobile.domain.Marca;
import br.com.usp.each.estacioneakimobile.domain.Modelo;
import br.com.usp.each.estacioneakimobile.domain.Tamanho;
import br.com.usp.each.estacioneakimobile.domain.Veiculo;
import br.com.usp.each.estacioneakimobile.service.ClienteService;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class EditarActivity extends Activity{
	ClienteService web = new ClienteService();
	private ProgressDialog mprogressDialog;
	List<Marca> marcas;
	private EditText txtNome;
	private EditText txtNovaSenha;
	private EditText txtPlaca;
	private EditText txtCor;
	private EditText txtTelefone;
	private EditText txtEmail;
	private Spinner spMarca;
	private Spinner spModelo;
	private Button btnEditar;
	private Button btnCancelar;
	private Cliente clienteCorrente;
	private static  boolean  marcaCarrega= false;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(br.com.usp.each.estacioneakimobile.R.layout.editar);
        
        Intent it = getIntent();
        clienteCorrente = (Cliente) it.getExtras().get(Cliente.class.getName());
        
        
        txtNome = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtNome);
        txtNovaSenha = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtNovaSenha);
        txtPlaca = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtPlaca);
        txtTelefone = (EditText)findViewById(br.com.usp.each.estacioneakimobile.R.id.txtTelefone);
        txtEmail = (EditText)findViewById(br.com.usp.each.estacioneakimobile.R.id.txtEmail);
        txtCor = (EditText) findViewById(br.com.usp.each.estacioneakimobile.R.id.txtCor);
        spMarca = (Spinner) findViewById(br.com.usp.each.estacioneakimobile.R.id.spMarca);
        spModelo = (Spinner) findViewById(br.com.usp.each.estacioneakimobile.R.id.spModelo);
        btnEditar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnEditar);
        btnCancelar = (Button) findViewById(br.com.usp.each.estacioneakimobile.R.id.btnCancelar);
        new ServiceGetMarcaeModelo().execute();
        
        
        spMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(!marcaCarrega){
					setSpModelo(position);
				}
				marcaCarrega=false;
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
        	
		});
   
        
        btnCancelar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it = new Intent(EditarActivity.this,MainActivity.class);
				it.putExtra(Cliente.class.getName(), clienteCorrente);
				startActivity(it);
				finish();
			}
		});
        
        btnEditar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clienteCorrente.setNome(txtNome.getText().toString());
				clienteCorrente.setTelefone(txtTelefone.getText().toString());
				
					if( (!txtNovaSenha.getText().toString().equals("")) && txtNovaSenha.getText().toString().length()>0 ){
						clienteCorrente.setSenha(txtNovaSenha.getText().toString());
					}
					else{
						clienteCorrente.setSenha(null);
					}
					clienteCorrente.setStatus(0);
					clienteCorrente.setEmail(txtEmail.getText().toString());
					clienteCorrente.getVeiculos().get(0); 
					clienteCorrente.getVeiculos().get(0).setCor(txtCor.getText().toString());
					clienteCorrente.getVeiculos().get(0).setMarca(marcas.get(spMarca.getSelectedItemPosition()));
					clienteCorrente.getVeiculos().get(0).setModelo(getModeloporMarca(clienteCorrente.getVeiculos().get(0).getMarca(), spModelo.getSelectedItem().toString()));
					clienteCorrente.getVeiculos().get(0).setPlaca(txtPlaca.getText().toString());
					
					if(clienteCorrente.getNome().length()>1 && clienteCorrente.getCpf_cnpj().length()>9
							&& clienteCorrente.getEmail().length()>1 && clienteCorrente.getTelefone().length()>1 &&
							clienteCorrente.getVeiculos().get(0).getCor().length()>1 &&
							clienteCorrente.getVeiculos().get(0).getPlaca().length()>1){
						new ServiceAtualizarCadastro().execute(clienteCorrente);
					}
					else{
						Toast.makeText(EditarActivity.this, "Todos os campos devem ser preenchidos exceto a senha , caso deseje mantela",Toast.LENGTH_SHORT).show();
					}
					
				
			}
		});
	}
	
	
	public Modelo getModeloporMarca(Marca marca, String nome){ 
		for(Modelo m : marca.getModelo()){
			if(m.getModelo().equals(nome)){
				return m;
			}
		}
		return null;
	}
	public void carregar(){
		txtNome.setText(clienteCorrente.getNome());
		txtNovaSenha.setText("");
		txtTelefone.setText(clienteCorrente.getTelefone());
		txtEmail.setText(clienteCorrente.getEmail());
		if(clienteCorrente.getVeiculos().size()>0){
			txtPlaca.setText(clienteCorrente.getVeiculos().get(0).getPlaca());
			txtCor.setText(clienteCorrente.getVeiculos().get(0).getCor());	
			
			spMarca.setSelection(getPositionMarca(clienteCorrente.getVeiculos().get(0).getMarca()));
			int positionModelo = setSpModelo(getPositionMarca(clienteCorrente.getVeiculos().get(0).getMarca()) ,clienteCorrente.getVeiculos().get(0).getModelo().getModelo());
			spModelo.setSelection(positionModelo);
			marcaCarrega = true;
		}
		
		
	}
	
	private int getPositionMarca(Marca marca){
		int contador = 0;
		for(Marca m : marcas ){
			if(m.getId().equals(marca.getId())){
				return contador;
			}
			contador++;
		}
		
		return contador;
	}
	
	
	
	public void setSpModelo(int idMarca){
		List<String> modeloString = new ArrayList<String>();
		if(marcas!=null){
			for(Modelo m: marcas.get(idMarca).getModelo()){
				modeloString.add(m.getModelo());
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, modeloString);
			arrayAdapter.notifyDataSetChanged();
			spModelo.setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		}
	}
	
	public int setSpModelo(int idMarca , String modelo){
		List<String> modeloString = new ArrayList<String>();
		int contador= 0;
		boolean flg = true;
		if(marcas!=null){
			for(Modelo m: marcas.get(idMarca).getModelo()){
				if(modelo.equals(m.getModelo())){
					flg= false;
				}
				modeloString.add(m.getModelo());
				
				if(flg){
					contador++;
				}
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, modeloString);
			arrayAdapter.notifyDataSetChanged();
			spModelo.setAdapter(arrayAdapter);
			arrayAdapter.notifyDataSetChanged();
		}
		return contador;
	}
	
	
	public void setSpMarca(){
		List<String> marcasString = new ArrayList<String>();
		if(marcas !=null){
			for(Marca m: marcas){
				marcasString.add(m.getMarca());
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, marcasString);
			spMarca.setAdapter(arrayAdapter);
		}
	}
	
	class ServiceGetMarcaeModelo extends AsyncTask<Void, String, List<Marca>>{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(EditarActivity.this, "Aguarde", "Processando...");
		}

		
		@Override
		protected void onPostExecute(List<Marca> result) {
			
			super.onPostExecute(result);
			marcas = result;
			  setSpMarca();
			  carregar();
			mprogressDialog.dismiss();
		}

		@Override
		protected List<Marca> doInBackground(Void... params) {
			List<Marca> result = null;
			try {
				result = web.getMarcas();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
	}
	
	
	class ServiceAtualizarCadastro extends AsyncTask<Cliente, String, Cliente>{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			mprogressDialog = ProgressDialog.show(EditarActivity.this, "Aguarde", "Processando...");
		}

		
		@Override
		protected void onPostExecute(Cliente result) {
			
			super.onPostExecute(result);
			clienteCorrente = result;
			if(clienteCorrente!=null){
				
				Toast.makeText(EditarActivity.this, "Cadastro atualizado com sucesso",Toast.LENGTH_SHORT).show();
				Intent it = new Intent(EditarActivity.this,MainActivity.class);
				it.putExtra(Cliente.class.getName(), clienteCorrente);
				startActivity(it);
				
				finish();
			}
			else{
				Toast.makeText(EditarActivity.this, "Ocorreu algum erro,tente novamente",Toast.LENGTH_SHORT).show();;
			}
			mprogressDialog.dismiss();
		}


		@Override
		protected Cliente doInBackground(Cliente... params) {
			Cliente result = null;
			try {
				result = web.atualizar(params[0]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return result;
		}
		
	}
}
